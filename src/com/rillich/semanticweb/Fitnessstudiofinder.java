package com.rillich.semanticweb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Fitnessstudiofinder {
	
	private String url = "http://www.fitnessstudiofinder.de/studios/Sachsen/Leipzig?adresse=leipzig&plzort=&lat=51.3396955&lon=12.3730747";
	private String htmlSplitBy=",";
	private String cvsSplitBy = ";";
	private boolean firstRow=true;
	private Timer waitTimer;
	
	private void initializeWaitTimer()
	{
		waitTimer = new Timer();
		waitTimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//wait for 1 second
				this.cancel();
			}
		}, 1000, 100000);
	}
	
	public void getFitnessstudios()
	{
		Document doc = null;
		
		MyFile file = new MyFile();
		String fileName = "fitnesscenter.csv";
		file.deletFile(fileName);
		
		try
		{
			doc = Jsoup.connect(url).get();
		}catch (Exception e)
		{
			System.out.println("Could not open Website: " +url);
		}
		
		try {
			file.writeTextFile("Studio;Adress;Postalcode;latitude;longitude", fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements studionamen = doc.select("h4");
		Elements adressen = doc.select("p");
		Element adresse;
		Element studioname;

		for(int i=0; i< studionamen.size(); i++)
		{
			studioname = studionamen.get(i);
			adresse = adressen.get(i);
			String[] adress = adresse.text().toString().split(htmlSplitBy);
			System.out.println(studioname.text()+ ";" + adress[0]+";"+adress[1]);
			String textToPrint = studioname.text()+ ";" + adress[0]+";"+adress[1];
			try {
				file.writeTextFile(textToPrint, fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void addGeoDataToCSV() throws IOException
	{
	
		GeoData geoData = new GeoData();
		
		try {
			File fileDir = new File("fitnesscenterb.csv");
	 
			BufferedReader in = new BufferedReader(
			   new InputStreamReader(new FileInputStream(fileDir), "UTF8"));	
			
			//StringBuilder sb = new StringBuilder();
	        String line = in.readLine();
	        
	        while (line != null) {

	        	if(firstRow)
	        	{
	        		line = in.readLine();
	        		firstRow=false;
	        	}
	        	else
	        	{	        		
		            line = in.readLine();
		            String[] adress = line.split(cvsSplitBy);
		            //System.out.println(adress[1]+" "+ adress[2]);
		            int end = adress[2].indexOf("Leipzig");
		            String substring = adress[2].substring(0, end-1);
		            System.out.println(adress[1]+ substring);
		            ArrayList<Float> longLat = geoData.getGeoData(adress[1], substring);
		            System.out.println(longLat.get(0)+";"+longLat.get(1));
		            
	        	}
	            
	        }
	 	 
	                in.close();
		    } 
		    catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
				
	}

}
