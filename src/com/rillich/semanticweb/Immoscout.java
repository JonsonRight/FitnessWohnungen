package com.rillich.semanticweb;

import java.io.File;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Immoscout {
	
	
	public void readApartments()
	{
		Document doc = null;
		File input = new File("apartmentsxml.txt");
		
		MyFile file = new MyFile();
		String fileName = "apartments.csv";
		file.deletFile(fileName);
		
		try {
			doc = Jsoup.parse(input, "UTF-8", "http://rest.immoscout24.com/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			file.writeTextFile("Apartment;Adress;Postalcode;latitude;longitude", fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Elements apartments = doc.select("address");
		Elements streets = doc.select("street");
		Elements housenumber = doc.select("houseNumber");
		Elements postcode = doc.select("postcode");
		Elements latidude = doc.select("latitude");
		Elements longitude = doc.select("longitude");
		
		String textToPrint;
		
		for(int i=0; i<streets.size(); i++)
		{
			textToPrint = "Wohnung "+(i+1)+";"+streets.get(i).text()+ " "+housenumber.get(i).text()+";"+postcode.get(i).text()+" Leipzig"+";"+latidude.get(i).text()+";"+longitude.get(i).text();
			//System.out.println(streets.get(i).text());
			try {
				file.writeTextFile(textToPrint, fileName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
