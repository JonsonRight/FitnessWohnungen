package com.rillich.semanticweb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Schwimmbadcheck {
	
	private String stringUrl = "http://www.schwimmbadcheck.de/schwimmbad/leipzig.html";
	private String cvsSplitBy = ";";
	private boolean firstRow=true;
	
	
	public void getAllPoolsFromSchwimmbadcheckWebsite()
	{
		parseWebsite(stringUrl);
	}
	
	private void parseWebsite(String url)
	{
		Document doc = null;
		MyFile file = new MyFile();
		String fileName = "pools.csv";
		file.deletFile(fileName);
		
		try
		{
			doc = Jsoup.connect(url).get();
		}catch (Exception e)
		{
			System.out.println("Could not open Website: " +url);
		}
		
		try {
			file.writeTextFile("Poolname;Adress;Postalcode", "pools.csv");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Elements table = doc.select("td");

		for(int i=0; i<table.size()-5; i++)
		{
			String splitString = table.get(i).text();
			int split = splitString.indexOf("Leipzig",0);
			if(split>0)
			{	
				split = splitString.indexOf("04",0);
				System.out.println(table.get(i-2).text()+";"+splitString.substring(0, split)+";"+splitString.substring(split, splitString.length()));
				String textToPrint = table.get(i-2).text()+";"+splitString.substring(0, split)+";"+splitString.substring(split, splitString.length());
				try {
					file.writeTextFile(textToPrint, fileName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}		
	}
	
	public void addGeoDataToCSV() throws IOException
	{
	
		GeoData geoData = new GeoData();
		
		try {
			File fileDir = new File("pools.csv");
	 
			BufferedReader in = new BufferedReader(
			   new InputStreamReader(new FileInputStream(fileDir), "UTF8"));	
			
			//StringBuilder sb = new StringBuilder();
	        String line = in.readLine();
	        
	        while (line != null) {

	        	if(firstRow)
	        	{
	        		line = in.readLine();
	        		firstRow=false;
	        	}
	        	else
	        	{
	        		//sb.append(line);
		            //sb.append(System.lineSeparator());
		            line = in.readLine();
		            String[] adress = line.split(cvsSplitBy);
		            //System.out.println(adress[1]+" "+ adress[2]);
		            int end = adress[2].indexOf("Leipzig");
		            String substring = adress[2].substring(0, end-1);
		            System.out.println(adress[1]+ substring);
		            ArrayList<Float> longLat = geoData.getGeoData(adress[1], substring);
		            System.out.println(longLat.get(0)+";"+longLat.get(1));
	        	}
	            
	        }
	 	 
	                in.close();
		    } 
		    catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
				
	}

}
