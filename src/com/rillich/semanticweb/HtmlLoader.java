package com.rillich.semanticweb;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlLoader {
	
	public String getCompleteWebsite(String url)
	{
		StringBuffer myString = new StringBuffer();
		try 
		{
		    String thisLine;
		    URL u = new URL(url);
		    DataInputStream theHTML = new DataInputStream(u.openStream());
		    
		    while ((thisLine = theHTML.readLine()) != null) 
		    {
		        myString.append(thisLine);
		    } 
		} 
		catch (MalformedURLException e) 
		{

		} 
		catch (IOException e) 
		{

		}
		
		return myString.toString();
	}
	
	public void getJavaScriptWebsite(String url)
	{
		Document doc = null;
		
		try
		{
			doc = Jsoup.parse(url, "UTF-8");
		}catch (Exception e)
		{
			System.out.println("Could not open Website: " +url);
		}
		
		Element element = doc.getElementsByTag("script").get(0);
	}
	
	

}
