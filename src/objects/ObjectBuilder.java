package objects;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.rillich.semanticweb.GeoData;

public class ObjectBuilder {
	
	private ArrayList<Fitnessstudio> fitnessstudios = new ArrayList<Fitnessstudio>();
	private ArrayList<Schwimmbad> schwimmbads = new ArrayList<Schwimmbad>();
	private ArrayList<Wohnung> wohnungen = new ArrayList<Wohnung>();
	
	public void startBuild()
	{
		createFitnessstudios();
		createPools();
		createWohnungen();
		addPoolsAndStudiosToApartments();
		Triplestore triplestore = new Triplestore(wohnungen);
	}
	
	private void createFitnessstudios()
	{
		String cvsSplitBy = ";";
		
		try {
			File fileDir = new File("fitnesscenterfinal.csv");
	 
			BufferedReader in = new BufferedReader(
			   new InputStreamReader(new FileInputStream(fileDir), "UTF8"));	
			
	        String line = in.readLine();
	        
	        while (line != null) {
        		
		            line = in.readLine();
		            String[] studio = line.split(cvsSplitBy);		            
		            fitnessstudios.add(new Fitnessstudio(studio[0], studio[1], studio[2], Double.parseDouble(studio[3]), Double.parseDouble(studio[4])));		            
	        }
	 	 
	                in.close();
		    } 
		    catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
		
//		for(int i=0; i<fitnessstudios.size(); i++)
//		{
//			System.out.println(fitnessstudios.get(i).getName() + " " + fitnessstudios.get(i).getLongitude() + " " + fitnessstudios.get(i).getLatidude());
//		}
				
	}
	
	private void createPools()
	{
		String cvsSplitBy = ";";
		
		try {
			File fileDir = new File("poolsfinal.csv");
	 
			BufferedReader in = new BufferedReader(
			   new InputStreamReader(new FileInputStream(fileDir), "UTF8"));	
			
	        String line = in.readLine();
	        
	        while (line != null) {
        		
		            line = in.readLine();
		            String[] pool = line.split(cvsSplitBy);		            
		            schwimmbads.add(new Schwimmbad(pool[0], pool[1], pool[2], Double.parseDouble(pool[3]), Double.parseDouble(pool[4])));
	        }
	 	 
	                in.close();
		    } 
		    catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
		
//		for(int i=0; i<schwimmbads.size(); i++)
//		{
//			System.out.println(schwimmbads.get(i).getName() + " " + schwimmbads.get(i).getLongitude() + " " + schwimmbads.get(i).getLatidude());
//		}
	}
	
	private void createWohnungen()
	{
		String cvsSplitBy = ";";
		
		try {
			File fileDir = new File("apartments.csv");
	 
			BufferedReader in = new BufferedReader(
			   new InputStreamReader(new FileInputStream(fileDir), "UTF8"));	
			
	        String line = in.readLine();
	        
	        while (line != null) {
        		
		            line = in.readLine();
		            String[] apartment = line.split(cvsSplitBy);		            
		            wohnungen.add(new Wohnung(apartment[0], apartment[1], apartment[2], Double.parseDouble(apartment[3]), Double.parseDouble(apartment[4])));
	        }
	 	 
	                in.close();
		    } 
		    catch (UnsupportedEncodingException e) 
		    {
				System.out.println(e.getMessage());
		    } 
		    catch (IOException e) 
		    {
				System.out.println(e.getMessage());
		    }
		    catch (Exception e)
		    {
				System.out.println(e.getMessage());
		    }
		
//		for(int i=0; i<wohnungen.size(); i++)
//		{
//			System.out.println(wohnungen.get(i).getName() + " " + wohnungen.get(i).getLongitude() + " " + wohnungen.get(i).getLatidude());
//		}
	}
	
	private void addPoolsAndStudiosToApartments()
	{
		GeoData geoData = new GeoData();
		//double distance = geoData.calculateDistance(wohnungen.get(0).getLatidude() ,wohnungen.get(0).getLongitude(), fitnessstudios.get(0).getLatidude(), fitnessstudios.get(0).getLongitude());
		//System.out.println("distance: " + distance);
		
		for(int i=0; i<wohnungen.size(); i++)
		{
			ArrayList<Fitnessstudio> fitnessTempList = new ArrayList<Fitnessstudio>();
			ArrayList<Schwimmbad> schwimmbadTempList = new ArrayList<Schwimmbad>();
			for(int j=0; j < fitnessstudios.size(); j++)
			{
				double distanceFiti = geoData.calculateDistance(wohnungen.get(i).getLatidude() ,wohnungen.get(i).getLongitude(), fitnessstudios.get(j).getLatidude(), fitnessstudios.get(j).getLongitude());
				if(distanceFiti <= 2000)//liegt im Radius von 2 km
				{
					fitnessTempList.add(fitnessstudios.get(j));
				}
			}
			
			for(int k=0; k<schwimmbads.size(); k++)
			{
				double distancePool = geoData.calculateDistance(wohnungen.get(i).getLatidude() ,wohnungen.get(i).getLongitude(), schwimmbads.get(k).getLatidude(), schwimmbads.get(k).getLongitude());
				if(distancePool <= 2000)//liegt im Radius von 2 km
				{
					schwimmbadTempList.add(schwimmbads.get(k));
				}
			}
			
			if(!fitnessTempList.isEmpty() && !schwimmbads.isEmpty())
			{
				wohnungen.get(i).setFitnesstudios(fitnessTempList);
				wohnungen.get(i).setSchwimmbaeder(schwimmbadTempList);
			}
		}
		
		//ausgabe wie viele objekte die eigenschaften erf�llen
		int counter = 0;
		
		for(int i=0; i< wohnungen.size(); i++)
		{
			
			if(!wohnungen.get(i).getFitnesstudios().isEmpty() && !wohnungen.get(i).getSchwimmbaeder().isEmpty())
			{
				counter++;
			}		
		}
		System.out.println(counter);
	}
}


