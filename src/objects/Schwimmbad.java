package objects;

public class Schwimmbad {
	
	private String name;
	private String street;
	private String postalcode;
	private double longitude;
	private double latidude;
	
	public Schwimmbad(String name, String street, String postalcode,
			double longitude, double latidude) {
		super();
		this.name = name;
		this.street = street;
		this.postalcode = postalcode;
		this.longitude = longitude;
		this.latidude = latidude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public double getLatidude() {
		return latidude;
	}

	public void setLatidude(float latidude) {
		this.latidude = latidude;
	}
	
	

}
